var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var autoPrefixer = require('gulp-autoprefixer');
// if node version is lower than v.0.1.2
require('es6-promise').polyfill();
// var frontnote = require('gulp-frontnote');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
gulp.task('sass',function(){
    var onError = function(err) {
        notify.onError({
            title:    "Gulp",
            subtitle: "Failure!",
            message:  "Error: <%= error.message %>",
            sound:    "Beep"
        })(err);

        this.emit('end');
    };
    return gulp.src(['src/css/**/*.scss'])
        .pipe(plumber({errorHandler: onError}))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoPrefixer())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({stream:true}))
        .pipe(notify({ // Add gulpif here
           title: 'Gulp',
           subtitle: 'success',
           message: 'Sass task',
           sound: "Pop"
        }));
});

gulp.task('js',function(){
    var onError = function(err) {
        notify.onError({
            title:    "Gulp",
            subtitle: "Failure!",
            message:  "Error: <%= error.message %>",
            sound:    "Beep"
        })(err);

        this.emit('end');
    };
    return gulp.src(['src/js/lib/**/*.js','src/js/main.js'])
        .pipe(plumber({errorHandler: onError}))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.reload({stream:true}))
        .pipe(notify({ // Add gulpif here
           title: 'Gulp',
           subtitle: 'success',
           message: 'JS task',
           sound: "Pop"
        }));
});


gulp.task('image', () =>
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
);

gulp.task('default',function(){
    browserSync.init({
        server: "./"
    });
    gulp.watch('src/js/**/*.js',['js']);
    gulp.watch('src/css/**/*.scss',['sass']);
    gulp.watch('*.html').on('change',reload);
    gulp.watch('src/images/*',['image']);
});
